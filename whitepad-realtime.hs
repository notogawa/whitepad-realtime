{-# LANGUAGE OverloadedStrings #-}
import Control.Exception ( fromException )
import Control.Concurrent ( MVar, newMVar, modifyMVar_, readMVar )
import Control.Monad ( forM_ )
import Data.Text ( Text )
import Data.ByteString ( ByteString )
import qualified Data.Text as T
import qualified Network.WebSockets as WS
import qualified Network.Wai.Handler.Warp as Warp
import qualified Network.Wai.Handler.WebSockets as WaiWS
import Control.Monad.IO.Class ( liftIO )

type Client = (Text, WS.Sink WS.Hybi10)

type ServerState = [Client]

newServerState :: ServerState
newServerState = []

numClients :: ServerState -> Int
numClients = length

clientExists :: Client -> ServerState -> Bool
clientExists client = any ((== fst client) . fst)

addClient :: Client -> ServerState -> ServerState
addClient client clients = client : clients

removeClient :: Client -> ServerState -> ServerState
removeClient client = filter ((/= fst client) . fst)

broadcast :: ByteString -> ServerState -> IO ()
broadcast message clients = do
  forM_ clients $ \(_, sink) -> WS.sendSink sink $ WS.binaryData message

realtime :: MVar ServerState -> WS.Request -> WS.WebSockets WS.Hybi10 ()
realtime state rq = do
  WS.acceptRequest rq
  WS.getVersion >>= liftIO . putStrLn . ("Client version: " ++)
  sink <- WS.getSink
  msg <- WS.receiveData
  liftIO $ print msg
  clients <- liftIO $ readMVar state
  case msg of
    _   | not (prefix `T.isPrefixOf` msg) ->
           WS.sendTextData ("Wrong announcement" :: Text)
        | clientExists client clients ->
            WS.sendTextData ("User already exists" :: Text)
        | otherwise -> do
             liftIO $ modifyMVar_ state $ return . addClient client
             talk state client
      where
        prefix = "Hi! I am "
        client = (T.drop (T.length prefix) msg, sink)

talk :: WS.Protocol p => MVar ServerState -> Client -> WS.WebSockets p ()
talk state client@(user, _) =
    flip WS.catchWsError catchDisconnect $ do
      msg <- WS.receiveData
      liftIO $ readMVar state >>= broadcast msg
      talk state client
    where
      catchDisconnect e = case fromException e of
                            Just WS.ConnectionClosed -> liftIO $ modifyMVar_ state $ return . removeClient client
                            _ -> return ()

main :: IO ()
main = do
  state <- newMVar newServerState
  let settings = Warp.defaultSettings
                 { Warp.settingsIntercept = WaiWS.intercept (realtime state) }
  Warp.runSettings settings undefined
